CREATE TABLE users(
    id bigint index not_null primary_key auto_increment,
    username varchar(255),
    password varchar(255),
    role int not_null
);

CREATE TABLE courses(
    id bigint index not_null primary_key auto_increment,
    capacity int,
    name varchar(255),
    teacher bigint
);

CREATE TABLE studentsCourses(
    studentId bigint,
    courseId bigint,
    FOREIGN KEY (studentId) REFERENCES users(id),
    FOREIGN KEY (courseId) REFERENCES courses(id)
);

CREATE TABLE teachersCourses(
    teacherId bigint,
    courseId bigint,
    FOREIGN KEY (teacherId) REFERENCES users(id),
    FOREIGN KEY (courseId) REFERENCES courses(id)
);
