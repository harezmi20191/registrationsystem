package com.example.demo.dao;

import java.util.List;

import com.example.demo.model.Course;
import com.example.demo.model.Teacher;
import com.example.demo.model.TeachersCoursesRelation;

public interface TeachersCoursesRepository {
	
	List<TeachersCoursesRelation> findAll();
	List<Course> findByTeacherId(Long teacherId);
	List<Teacher> findByCourseId(Long courseId);
	void create(Long teacherId, Long courseId);
	void deleteRow(Long teacherId, Long courseId);
	void deleteTeacherRows(Long teacherId);
	void deleteCourseRows(Long courseId);

}
