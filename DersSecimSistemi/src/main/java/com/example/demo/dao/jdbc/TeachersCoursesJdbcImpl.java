package com.example.demo.dao.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import com.example.demo.dao.TeachersCoursesRepository;
import com.example.demo.model.Course;
import com.example.demo.model.StudentsCoursesRelation;
import com.example.demo.model.Teacher;
import com.example.demo.model.TeachersCoursesRelation;
import com.example.demo.model.User;

public class TeachersCoursesJdbcImpl implements TeachersCoursesRepository {
	
	
	@Autowired
	private JdbcTemplate jdbcTemplate;

	private RowMapper<TeachersCoursesRelation> relationRowMapper = new RowMapper<TeachersCoursesRelation>() {

		public TeachersCoursesRelation mapRow(ResultSet rs, int rowNum) throws SQLException {
			TeachersCoursesRelation teachersCoursesRelation = new TeachersCoursesRelation();
			
			teachersCoursesRelation.setTeacherId(rs.getLong("teacherId"));
			teachersCoursesRelation.setCourseId(rs.getLong("courseId"));
			
			return teachersCoursesRelation;
		}
	};
	
	private RowMapper<User> userRowMapper = new RowMapper<User>() {

		public User mapRow(ResultSet rs, int rowNum) throws SQLException {
			User user = new User();

			user.setId(rs.getLong("id"));
			user.setUsername(rs.getString("username"));
			user.setPassword(rs.getString("password"));
			user.setRole(rs.getInt("role"));
			
			
			return user;
		}
	};
	
	private RowMapper<Course> courseRowMapper = new RowMapper<Course>() {

		@Override
		public Course mapRow(ResultSet rs, int rowNum) throws SQLException {
			Course course = new Course();
			
			course.setId(rs.getLong("id"));
			course.setName(rs.getString("name"));
			course.setCapacity(rs.getInt("capacity"));
			course.setTeacher(rs.getLong("teacher"));
			
			return course;
		}
	};
	
	
	@Override
	public List<TeachersCoursesRelation> findAll() {
		String sql = "select teacherId,courseId from teachersCourses";
		
		return jdbcTemplate.query(sql, relationRowMapper);
	}

	@Override
	public List<Course> findByTeacherId(Long teacherId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Teacher> findByCourseId(Long courseId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void create(Long teacherId, Long courseId) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		PreparedStatementCreator psc = new PreparedStatementCreator() {
			
			@Override
			public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
				PreparedStatement stmt = con.prepareStatement(
						"insert into studentsCourses(teacherId,courseId) "
						+ " values(?,?)");
				stmt.setLong(1,teacherId);
				stmt.setLong(2,courseId);
				return stmt;
			}
		};
		int count = jdbcTemplate.update(psc, keyHolder);
		if(count != 1) {
			throw new RuntimeException("Unable to create teacher course relationship with teacherId :" + teacherId + ", and courseId :" + courseId);
		}
	}

	@Override
	public void deleteRow(Long teacherId, Long courseId) {
		String sql = "delete from teachersCourses where teacherId = ? and courseId = ?";
		jdbcTemplate.update(sql,teacherId, courseId);
	}

	@Override
	public void deleteTeacherRows(Long teacherId) {
		String sql = "delete from teachersCourses where teacherId = ?";
		jdbcTemplate.update(sql,teacherId);
	}

	@Override
	public void deleteCourseRows(Long courseId) {
		String sql = "delete from studentsCourses where courseId = ?";
		jdbcTemplate.update(sql,courseId);
	}

}
