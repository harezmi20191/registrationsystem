package com.example.demo.dao;

import java.util.List;

import com.example.demo.model.Course;

public interface CoursesRepository {

		List<Course> findAll();
		Course findById (Long id);
		void create(Course course);
		void delete(Long id);
		Course update(Course course);
		
		
}
