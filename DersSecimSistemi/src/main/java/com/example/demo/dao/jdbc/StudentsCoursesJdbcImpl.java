package com.example.demo.dao.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import com.example.demo.dao.StudentsCoursesRepository;
import com.example.demo.model.Course;
import com.example.demo.model.Student;
import com.example.demo.model.StudentsCoursesRelation;
import com.example.demo.model.User;

public class StudentsCoursesJdbcImpl implements StudentsCoursesRepository {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	private RowMapper<StudentsCoursesRelation> relationRowMapper = new RowMapper<StudentsCoursesRelation>() {

		public StudentsCoursesRelation mapRow(ResultSet rs, int rowNum) throws SQLException {
			StudentsCoursesRelation studentsCoursesRelation = new StudentsCoursesRelation();
			
			studentsCoursesRelation.setStudentId(rs.getLong("studentId"));
			studentsCoursesRelation.setCourseId(rs.getLong("courseId"));
			
			return studentsCoursesRelation;
		}
	};
	
	private RowMapper<User> userRowMapper = new RowMapper<User>() {

		public User mapRow(ResultSet rs, int rowNum) throws SQLException {
			User user = new User();

			user.setId(rs.getLong("id"));
			user.setUsername(rs.getString("username"));
			user.setPassword(rs.getString("password"));
			user.setRole(rs.getInt("role"));
			
			
			return user;
		}
	};
	
	private RowMapper<Course> courseRowMapper = new RowMapper<Course>() {

		@Override
		public Course mapRow(ResultSet rs, int rowNum) throws SQLException {
			Course course = new Course();
			
			course.setId(rs.getLong("id"));
			course.setName(rs.getString("name"));
			course.setCapacity(rs.getInt("capacity"));
			course.setTeacher(rs.getLong("teacher"));
			
			return course;
		}
	};
	
	@Override
	public List<StudentsCoursesRelation> findAll() {
		String sql = "select studentId,courseId from studentsCourses";
		
		return jdbcTemplate.query(sql, relationRowMapper);
	}

	
	// SORULACAK !!!
	@Override
	public List<Course> findByStudentId(Long studentId) {
		// TODO Auto-generated method stub
		return null;
	}

	// SORULACAK !!!
	@Override
	public List<Student> findByCourseId(Long courseId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void create(Long studentId, Long courseId) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		PreparedStatementCreator psc = new PreparedStatementCreator() {
			
			@Override
			public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
				PreparedStatement stmt = con.prepareStatement(
						"insert into studentsCourses(studentId,courseId) "
						+ " values(?,?)");
				stmt.setLong(1,studentId);
				stmt.setLong(2,courseId);
				return stmt;
			}
		};
		int count = jdbcTemplate.update(psc, keyHolder);
		if(count != 1) {
			throw new RuntimeException("Unable to create student course relationship with studentId :" + studentId + ", and courseId :" + courseId);
		}
	}

	@Override
	public void deleteRow(Long studentId, Long courseId) {
		String sql = "delete from studentsCourses where studentId = ? and courseId = ?";
		jdbcTemplate.update(sql,studentId, courseId);
	}

	@Override
	public void deleteStudentRows(Long studentId) {
		String sql = "delete from studentsCourses where studentId = ?";
		jdbcTemplate.update(sql,studentId);
	}

	@Override
	public void deleteCourseRows(Long courseId) {
		String sql = "delete from studentsCourses where courseId = ?";
		jdbcTemplate.update(sql,courseId);
	}

}
