package com.example.demo.dao.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import com.example.demo.dao.CoursesRepository;
import com.example.demo.model.Course;

public class CoursesJdbcImpl implements CoursesRepository {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	private RowMapper<Course> rowMapper = new RowMapper<Course>() {

		@Override
		public Course mapRow(ResultSet rs, int rowNum) throws SQLException {
			Course course = new Course();
			
			course.setId(rs.getLong("id"));
			course.setName(rs.getString("name"));
			course.setCapacity(rs.getInt("capacity"));
			course.setTeacher(rs.getLong("teacher"));
			
			return course;
		}
	};
	
	@Override
	public List<Course> findAll() {
		String sql = "select id,name,capacity,teacher from courses";
		
		return jdbcTemplate.query(sql, rowMapper);
	}

	@Override
	public Course findById(Long id) {
		String sql = "select id,name,capacity,teacher from courses where id = ?";
		return DataAccessUtils.singleResult(jdbcTemplate.query(sql, rowMapper, id));
	}

	@Override
	public void create(Course course) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		PreparedStatementCreator psc = new PreparedStatementCreator() {
			
			@Override
			public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
				PreparedStatement stmt = con.prepareStatement(
						"insert into courses(id,name,capacity,teacher) "
						+ " values(?,?,?,?)");
				
				stmt.setLong(1, course.getId());
				stmt.setString(2, course.getName());
				stmt.setInt(3, course.getCapacity());
				stmt.setLong(4, course.getTeacher());
				return stmt;
			}
		};
		int count = jdbcTemplate.update(psc, keyHolder);
		if(count != 1) {
			throw new RuntimeException("Unable to create course :" + course);
		}
		course.setId((Long) keyHolder.getKey());
	}

	@Override
	public void delete(Long id) {
		String sql = "delete from courses where id = ?";
		jdbcTemplate.update(sql,id);
	}

	@Override
	public Course update(Course course) {
		int count = jdbcTemplate.update("update courses "
				+ "set id = ?, name = ? " + " , capacity = ? , teacher = ? "
				+ "where id = ?", course.getId(), course.getName(), course.getCapacity(), course.getTeacher());
		if(count != 1) {
			throw new RuntimeException("Unable to update course :" + course);
		}
		return course;
	}

}
