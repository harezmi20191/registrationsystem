package com.example.demo.dao;

import java.util.List;

import com.example.demo.model.Course;
import com.example.demo.model.Student;
import com.example.demo.model.StudentsCoursesRelation;

public interface StudentsCoursesRepository {

	List<StudentsCoursesRelation> findAll();
	List<Course> findByStudentId(Long studentId);
	List<Student> findByCourseId(Long courseId);
	void create(Long studentId, Long courseId);
	void deleteRow(Long studentId, Long courseId);
	void deleteStudentRows(Long studentId);
	void deleteCourseRows(Long courseId);
	
}
