package com.example.demo.dao.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import com.example.demo.dao.UsersRepository;
import com.example.demo.model.User;

public class UsersJdbcImpl implements UsersRepository {

	
	@Autowired
	private JdbcTemplate jdbcTemplate;

	private RowMapper<User> rowMapper = new RowMapper<User>() {

		public User mapRow(ResultSet rs, int rowNum) throws SQLException {
			User user = new User();

			user.setId(rs.getLong("id"));
			user.setUsername(rs.getString("username"));
			user.setPassword(rs.getString("password"));
			user.setRole(rs.getInt("role"));
			
			
			return user;
		}
	};
	
	
	@Override
	public List<User> findAll() {
		String sql = "select id,username,password,role from users";
		
		return jdbcTemplate.query(sql, rowMapper);
	}

	@Override
	public User findById(Long id) {
		String sql = "select id, username,password,role from users where id = ?";
		return jdbcTemplate.queryForObject(sql, User.class, ""+id);
	}

	@Override
	public void create(User user) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		PreparedStatementCreator psc = new PreparedStatementCreator() {
			
			@Override
			public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
				PreparedStatement stmt = con.prepareStatement(
						"insert into users(id,username,password,role) "
						+ " values(user_sequence.nextval,?,?,?)");
				stmt.setString(1,user.getUsername());
				stmt.setString(2,user.getPassword());
				stmt.setInt(3, user.getRole());
				return stmt;
			}
		};
		int count = jdbcTemplate.update(psc, keyHolder);
		if(count != 1) {
			throw new RuntimeException("Unable to create user :" + user);
		}
		user.setId((Long) keyHolder.getKey());
	}
	
	

	@Override
	public User update(User user) {
		int count = jdbcTemplate.update("update users "
				+ "set id = ?, username = ? "
				+ ",password = ?," + "role = ?",user.getId(),user.getUsername(),user.getPassword(), user.getRole());
		if(count != 1) {
			throw new RuntimeException("Unable to update owner :" + user);
		}
		return user;
	}

	@Override
	public void delete(Long id) {
		String sql = "delete from users where id = ?";
		jdbcTemplate.update(sql,id);
	}

}
