package com.example.demo.dao;

import java.util.List;

import com.example.demo.model.User;

public interface UsersRepository {

	List<User> findAll();
	User findById(Long id);
	void create(User user);
	User update(User user);
	void delete(Long id);
	
}
