package com.example.demo.model;

import java.util.List;

public class Teacher extends User {

	List<Course> courses;

	public List<Course> getCourses() {
		return courses;
	}

	public void setCourses(List<Course> courses) {
		this.courses = courses;
	}
	
}
