package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;

import com.example.demo.dao.CoursesRepository;
import com.example.demo.dao.TeachersCoursesRepository;
import com.example.demo.dao.UsersRepository;
import com.example.demo.model.Course;
import com.example.demo.model.User;

public class SystemAdminServiceImpl implements SystemAdminService {

	private UsersRepository usersRepository;
	private CoursesRepository coursesRepository;
	private TeachersCoursesRepository teachersCoursesRepository;
	
	@Autowired
	public void setUsersRepository(UsersRepository usersRepository) {
		this.usersRepository = usersRepository;
	}

	@Autowired
	public void setCoursesRepository(CoursesRepository coursesRepository) {
		this.coursesRepository = coursesRepository;
	}

	@Autowired
	public void setTeachersCoursesRepository(TeachersCoursesRepository teachersCoursesRepository) {
		this.teachersCoursesRepository = teachersCoursesRepository;
	}

	@Override
	public void userAdd(User user) {
		usersRepository.create(user);
	}

	@Override
	public void userDelete(Long userId) {
		usersRepository.delete(userId);
	}

	@Override
	public void roleChange(Long userId, int roleCode) {
		User user = usersRepository.findById(userId);
		user.setRole(roleCode);
		usersRepository.update(user);
	}

	@Override
	public void addCourse(Course course) {
		coursesRepository.create(course);
	}

	@Override
	public void setTeacher(Long teacherId, Long courseId) {
		teachersCoursesRepository.create(teacherId, courseId);
	}

}
