package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;

import com.example.demo.dao.CoursesRepository;
import com.example.demo.model.Course;

public class TeacherServiceImpl implements TeacherService {

	private CoursesRepository coursesRepository;
	
	@Autowired
	public void setCoursesRepository(CoursesRepository coursesRepository) {
		this.coursesRepository = coursesRepository;
	}

	@Override
	public void increaseCourseCapacity(Long courseId, int increaseCapacity) {
		Course course = coursesRepository.findById(courseId);
		int capacity = course.getCapacity();
		course.setCapacity(capacity + increaseCapacity);
		coursesRepository.update(course);
	}

}
