package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;

import com.example.demo.dao.StudentsCoursesRepository;

public class StudentServiceImpl implements StudentService {

	private StudentsCoursesRepository studentsCoursesRepository;
	
	@Autowired
	public void setStudentsCoursesRepository(StudentsCoursesRepository studentsCoursesRepository) {
		this.studentsCoursesRepository = studentsCoursesRepository;
	}

	@Override
	public void enroll(Long studentId, Long courseId) {
		studentsCoursesRepository.create(studentId, courseId);
	}

	@Override
	public void drop(Long studentId, Long courseId) {
		studentsCoursesRepository.deleteRow(studentId, courseId);
	}

}
