package com.example.demo.service;

import com.example.demo.model.Course;
import com.example.demo.model.User;

public interface SystemAdminService {

	public void userAdd(User user);
	public void userDelete(Long userId);
	public void roleChange(Long userId, int roleCode);
	public void addCourse(Course course);
	public void setTeacher(Long teacherId, Long courseId);
	
}
