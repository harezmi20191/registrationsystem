package com.example.demo.service;

public interface StudentService {

	public void enroll(Long studentId, Long courseId);
	public void drop(Long studentId, Long courseId);
	
}
