package com.example.demo.service;

public interface TeacherService {

	public void increaseCourseCapacity(Long courseId, int increaseCapacity);
	
}